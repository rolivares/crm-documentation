## Modulo de Cliente

Los clientes son la parte más importante de la aplicación. El objetivo principal de esta aplicación es servir a los clientes. El área de clientes está separada del área de administración. Tienen su propio portal y pueden administrar fácilmente tickets, facturas, contratos, presupuestos.

### Nuevo cliente

Inicie sesión en el panel de control, haga clic en clientes y en el lado izquierdo hay un botón Nuevo cliente.

- **Permisos** : qué permiso tendrá este cliente en el portal de clientes. Por ejemplo, si apaga el sistema de soporte, el cliente no podrá utilizar el sistema de soporte. Mientras esté en tubería podrá usarlo.

- **Facturación y envío** : esta información se utiliza para facturas / estimaciones. Si su cliente tiene una dirección de envío diferente para la factura, deberá completar también los campos de envío que también se incluirán automáticamente al crear una nueva factura.

- **Opciones avanzadas** : como opciones avanzadas se enumeran varios campos.

    - Grupos: clasifique a su cliente en grupos, después de lo cual también podrá generar informes por grupos y ver qué grupos de clientes son los más valiosos.

    - Moneda: si esta moneda no es la moneda predeterminada del sistema cuando agrega el presupuesto / factura y selecciona el cliente, la moneda se cambiará automáticamente con respecto a lo que se seleccione en este campo. NOTA : No podrá cambiar la moneda personalizada si se encuentran transacciones.

    - Idioma: si tiene varios idiomas, puede cambiar fácilmente el idioma del cliente. Por lo tanto, cuando el cliente inicie sesión y configure un idioma diferente, el sistema se traducirá de forma predeterminada en función de su traducción para este idioma.

        - De forma predeterminada, todos los datos PDF del cliente generados desde el área de administración se generan en el idioma predeterminado del sistema. Si desea generar en el idioma del cliente, puede ir fácilmente a Configuración-> Configuración-> Localización y establecer  Salida de documentos PDF del cliente desde el área de administración en el idioma del cliente en Sí

Complete todos los datos necesarios y haga clic en enviar.
También puede elegir si desea enviar la plantilla de correo electrónico del cliente de bienvenida, que está configurada en el menú Configuración -> Plantillas de correo electrónico en la parte de clientes.

Ahora su cliente puede iniciar sesión en el área de clientes desde la url www.yourdomain.com/clients/login

### Registro De Clientes

El registro de cliente está deshabilitado, solo podrá agregar clientes desde el área de administración.

### Descripción General Del Cliente

Puede ver todos los datos del cliente, como facturas, contratos, tickets, notas, haciendo clic en el botón de edición del cliente y en el sitio correcto puede ver toda la actividad del cliente en su empresa, también hay una opción Iniciar sesión como cliente para que pueda verificar su perfil del cliente.

### Exportación De Facturas De Clientes / Estimaciones / Pago A ZIP

Vaya al perfil del cliente y haga clic en la pestaña que desea exportar los datos eq. facturas

Seleccione el estado o simplemente déjelo Todo y haga clic en Enviar. Todas las facturas basadas en las opciones seleccionadas se agregarán al archivo .zip.

![imagen importarFacturaCliente](img/img_1.png)

### Enviar Enlace De Contraseña Para Establecer Contacto

Siga los pasos si desea enviar el enlace de configuración de contraseña a su contacto.

- Al crear un nuevo contacto

Cuando agregue un nuevo contacto, solo marque el campo Enviado el enlace de la contraseña SET y no complete el campo de la contraseña.
Una vez creado el contacto, se enviará el correo electrónico para establecer el enlace de contraseña.

- Cuando el contacto ya está creado.

Solo marque el campo Enviado el enlace de la contraseña SET y haga clic en guardar. Se enviará un correo electrónico al contacto.

NOTA: Este enlace caducará después de 48 horas.

Debe haber configurado el correo electrónico SMTP para poder utilizar esta función.

### Nuevo Contacto Con El Cliente

Haga clic en Clientes en la pestaña Menú y seleccione Contratos.

Para crear un nuevo contacto, haga clic en el botón **Nuevo contacto** .

![imagen nuevoContacto](img/img_2.png)