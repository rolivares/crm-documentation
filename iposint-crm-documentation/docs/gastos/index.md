# Modulo de gastos

## Gastos De Registro

Para agregar un nuevo gasto, haga clic en el menú lateral Gastos y Registro de gastos.

De forma predeterminada, el monto del gasto estará en su moneda base . Si elige que este gasto sea facturable, la moneda se cambiará de acuerdo con la moneda del cliente.

**Nota**: si la moneda del cliente es la predeterminada del sistema, la moneda permanecerá como la moneda base definida.

**Más información sobre moneda**

- Cuando registra un gasto para su empresa (no para el cliente), se utiliza la moneda base.
- Cuando registra un gasto para el cliente y no es facturable, se utiliza la moneda base.
- Cuando registra un gasto para el cliente y es facturable, se usa la moneda del cliente

## Gasto Facturable

Puede crear gastos facturables a sus clientes. Al agregar un nuevo gasto, seleccione el cliente y se mostrará la nueva casilla de verificación. Marque la casilla de verificación facturable y podrá convertir este gasto en factura.

De forma predeterminada, el monto del gasto estará en su moneda base. Si elige que este gasto sea facturable, la moneda se cambiará de acuerdo con la moneda del cliente.

Tenga en cuenta que si la moneda del cliente es la predeterminada del sistema, la moneda permanecerá como la moneda base definida.

![imagen gastos](img/img_1.png)

## Conversión De Gastos Facturables En Factura

Puede convertir el gasto en factura si el gasto es facturable. Después de crear el gasto facturable, podrá ver el botón Convertir en factura o simplemente puede convertirlo y guardarlo como borrador. Haga clic y la factura con toda la información se creará automáticamente.

![imagen gastos](img/img_2.png)

## Creación De Gastos Recurrentes

Establecer gastos de recuperación es muy fácil. En el lado derecho, Opción avanzada, puede establecer cuándo se repetirá este gasto. También se permite la opción personalizada para adaptarse a sus necesidades.

![imagen gastos](img/img_3.png)

## Reporte De Gastos

Hay 3 tipos de informes de gastos.

- Informe anual de todos los gastos, incluidas las categorías.
    - Para ver el informe de gastos completo, vaya a **Informes -> Gastos**
- Reporte detallado.
    - Para ver el informe detallado de los gastos en los que se incluye el IMPUESTO y el IMPUESTO TOTAL con la capacidad de filtrar, vaya a **Informes-> Gastos** y haga clic en el botón **Informe detallado**
- Informe de gastos frente a ingresos.
    - Para ver el informe de gastos frente a ingresos, vaya a **Informes -> Gastos frente a ingresos**
El informe de gastos frente a ingresos se muestra en su moneda base. Si tiene pagos en otra moneda, el resultado no será exacto al 100%
