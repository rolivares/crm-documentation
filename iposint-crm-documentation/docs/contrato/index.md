# Contratos

Puede agregar nuevos contratos basados ​​en sus clientes. Agregar contratos es muy simple, puede establecer la fecha de inicio y la fecha de finalización y tener una vista clara de todos los contratos de su empresa en un solo lugar.
Ya no necesitará buscar en los documentos de su escritorio.

Crear un PDF de contrato y enviarlo por correo electrónico es realmente sencillo.

Se permiten tipos de contrato, rastreando los contratos totales por tipo o por valor de tipo.

Tener la capacidad de renovar el contrato y realizar un seguimiento del historial de renovación del contrato.

## Nuevo Contrato

Haga clic en el enlace Contratos del menú lateral y en el lado izquierdo hay un botón Nuevo contrato.

Establezca el tema de los contratos pero tenga en cuenta que el tema solo es visible para el cliente.
La carga de archivos es opcional.

Si carga el archivo, este archivo también será visible para el lado del cliente y el cliente puede descargar el contrato.

## Renovación De Contrato

Puede renovar el contacto haciendo clic en editar contrato y en la parte superior derecha haga clic en Historial de renovación de contrato y haga clic en Renovar contrato.

![imagen contratos](img/img_1.png)

Después de ese modal se abrirá y los valores se completarán automáticamente. Solo necesita verificar los valores y luego hacer clic en guardar.

![imagen contratos](img/img_2.png)

Después de guardar en el lado derecho, puede realizar un seguimiento de todas las renovaciones de contratos.