# Tareas

## Crear nueva tarea

Para agregar una nueva tarea, haga clic en el menú lateral tareas y nueva tarea.

![imagen proyectos](img/img_1.png)

Después se despliega un modal donde tendra que llenar los datos solicitados.

![imagen proyectos](img/img_2.png)

## Agregar Asignatarios De Tareas

Para agregar asignatarios de tareas, abra la tarea haciendo clic en el tema de la tarea mientras se enumeran las tareas.

En el lado derecho del modal de la tarea, habrá un menú desplegable para seleccionar los asignados.

![imagen proyectos](img/img_3.png)