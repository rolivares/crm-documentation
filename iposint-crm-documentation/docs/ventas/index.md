# Modulo de ventas

## Propuestas

### Crear propuesta

En la parte superior izquierda, haga clic en  Nueva propuesta.

Puede crear una propuesta para llevar o para el cliente.

- Elija Relacionado con el cliente potencial o el cliente.

- Elija qué cliente potencial / cliente está relacionado . En el lado derecho, la información encontrada se completará automáticamente. Puede ajustar según sus necesidades.

- Estado: de  forma predeterminada, el estado del borrador se seleccionará automáticamente. Tenga en cuenta que las propuestas con estado de borrador no son visibles para los clientes.

- Abierto hasta : la fecha en la que la propuesta permanecerá abierta y el usuario puede realizar acciones (Aceptar / Rechazar). Vaya a Configuración-> Configuración-> Finanzas-> Propuesta para establecer la propuesta que vence después de completar automáticamente este campo.

- Moneda : al crear una propuesta para el cliente potencial, puede agregar cualquier moneda que necesite; de ​​lo contrario, si está creando una propuesta para el cliente , se utilizará la moneda base del sistema o la moneda del cliente .

- Permitir comentarios: si este campo no está marcado, el usuario no podrá comentar sobre la propuesta, ej. Negociar con precio, etc.

- Guarde la propuesta para agregar contenido a la propuesta.

### Conversión De Propuesta En Presupuesto / Factura

Para convertir la propuesta en factura o presupuesto, seleccione la propuesta de la tabla y en el lado derecho se mostrará un botón de conversión.

![imagen convertirpesupuesto](img/img_1.png)

Haga clic en el botón y elija presupuesto o Factura. Después de elegir, se mostrará una ventana emergente con la información de vista previa también con los elementos analizados que se insertan automáticamente en el presupuesto / factura.

Cuando esté satisfecho con el resultado, haga clic en Guardar.

**NOTA**: Si la propuesta está relacionada con algún prospecto que no se convierte en cliente, primero deberá convertir el prospecto en cliente y luego volver aquí y convertir la propuesta en presupuesto o factura.

### Envío De Propuesta Al Cliente O Cliente Potencial

Suponemos que ya ha creado la propuesta. Haga clic en el icono de correo desde el lado derecho y se mostrará una ventana emergente con la plantilla de correo electrónico de vista previa.

Puede incluir el enlace de la propuesta dentro de la plantilla de correo electrónico y dejar que su cliente lo vea sin iniciar sesión. Esto también es útil para clientes potenciales.

Después de enviar la propuesta, un cliente potencial / cliente puede abrirla y hacer una acción aceptar, rechazar, hacer comentarios. El creador de la propuesta y el agente de ventas asignado recibirán una notificación en el CRM y un correo electrónico de que el cliente comentó o acepta / rechaza la propuesta.

## Presupuesto

### Crear Presupuesto

Para crear una nueva Presupuesto, **vaya a Ventas -> Presupuesto -> Crear nueva Presupuesto o haga clic en el icono Generar en la parte superior izquierda y haga clic en Crear Presupuesto**

![imagen importarFacturaCliente](img/img_2.png)

**Cliente** : antes de seleccionar un cliente, debe haber agregado clientes Clientes-> Nuevo cliente.

Al seleccionar un Cliente, la información del cliente se obtendrá directamente del perfil de cliente que creó . Otra opción es que puede hacer clic en el icono de edición y agregar directamente la información del cliente deseada, esto también se aplica a la información de facturación que también se obtiene al seleccionar un cliente.

**NOTA**: Si desea que los detalles de facturación se completen automáticamente al seleccionar el cliente, debe haber agregado previamente los detalles de facturación en el perfil del cliente.

![imagen importarFacturaCliente](img/img_3.png)

**Facturación y envío**:  si cambia la información de facturación y envío después de seleccionar un cliente y hace clic en el icono de edición, esto se cambiará para la estimación, pero en el perfil del cliente la información seguirá siendo la misma y no habrá ningún cambio en la factura.

![imagen importarFacturaCliente](img/img_4.png)

**Número**  : el número se completa automáticamente Eq. 001 siguiente será 002.

**Estado**: de forma predeterminada, se seleccionará el estado del borrador, puede cambiar el estado en cualquier momento que desee, tenga en cuenta que cuando envíe el presupuesto a su cliente, el estado cambiará automáticamente a ENVIADO.

**Moneda**  : la moneda se selecciona automáticamente en función de su moneda predeterminada o la moneda configurada para el Cliente seleccionado que seleccionó anteriormente en el perfil del cliente. Si la moneda de este cliente es USD , se seleccionará automáticamente y no se puede cambiar.

**Agente de ventas**: puede seleccionar un agente de ventas para este presupuesto y luego generar informes en Informes-> Ventas-> Informe de presupuesto . Tenga en cuenta que el nombre completo del agente de ventas se mostrará en el presupuesto.

La nota de administrador es solo para administradores / miembros del personal.

La nota del cliente también es visible para el lado del cliente, en el presupuesto HTML y PDF.

### Conversión De Presupuesto En Factura

Puede convertir fácilmente el presupuesto en factura desde el área de administración.

Abra el presupuesto que desea convertir y, en el lado derecho, haga clic en Convertir en factura

![imagen importarFacturaCliente](img/img_5.png)

## Facturas

Las facturas son una parte importante de la gestión de una empresa. Perfex le permite realizar un seguimiento de sus facturas, artículos y generar informes. Agregar nuevas monedas, se permite el uso de varias monedas. Factura con diferente impuesto según artículo. Cree facturas recurrentes que se volverán a generar automáticamente en función de su configuración para la factura recurrente (cada X meses). Puede establecer la recurrencia a partir de 1 mes a 12 meses.

### Crear Factura

Para crear una nueva factura vaya a Ventas -> Facturas  y haga clic en el botón  Crear factura

![imagen importarFacturaCliente](img/img_6.png)

Seleccione **Cliente**, al seleccionar un Cliente, la información del cliente se obtendrá directamente del perfil del cliente. Otra opción es que puede hacer clic en el icono de edición y agregar directamente la información del cliente deseada, esto también se aplica a la información de facturación que también se obtiene al seleccionar un cliente.

![imagen importarFacturaCliente](img/img_7.png)

**NOTA**: Si desea que los detalles de facturación se completen automáticamente al seleccionar el cliente, debe haber agregado previamente los detalles de facturación en el perfil del cliente.

**Facturación y envío**:  si cambia la información de facturación y envío después de seleccionar un cliente y hace clic en el icono de edición, se cambiará para la factura, pero en el perfil del cliente la información seguirá siendo la misma.

![imagen importarFacturaCliente](img/img_4.png)

### Factura Recurrente

Vaya al menú de **configuración -> Configuración -> Cron Job**

![imagen importarFacturaCliente](img/img_8.png)

**¿Crear nueva factura a partir de la factura recurrente principal solo si la factura principal tiene el estado pagado?**

Si este campo se establece en No, la factura recurrente no tiene el estado pagado, no se creará la nueva factura.

**Enviar automáticamente la factura renovada al cliente**

Enviar después de renovar la factura enviar la factura al cliente. También se adjuntará el PDF

**NOTA** : Los contactos de los clientes deben tener permiso para que las facturas reciban el correo electrónico.

**Crear factura recuperable**

Cuando agrega / edita una factura, tiene la opción de configurar esta factura para que sea recurrente.

Eso significa que la factura se volverá a crear automáticamente en función de su configuración para la factura recurrente (cada X meses). Puede establecer recurrentes a partir de 1 mes a 12 meses o seleccionar personalizado para adaptarse a sus necesidades.

**¿Cómo se calculan los días recurrentes?**

La fecha en la que es necesario volver a crear (renovar) la factura se calcula a partir de la fecha de la factura.

Las fechas de los ejemplos se muestran en formato Ymd.

**Ejemplo 1**:

- La fecha de la factura es XXXX-08-17
- Recurrente es cada 1 mes
- La factura se volverá a crear (renovar) el XXXX-09-17

**Ejemplo 2**:

- La fecha de la factura es XXXX-03-01
- Recurrente es todos los meses
- La factura se volverá a crear (renovar) el XXXX-04-01

