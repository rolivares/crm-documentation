# Cliente Potencial

Clientes potenciales son una parte realmente importante para cualquier empresa. Todas las empresas intentan todos los días obtener nuevos clientes potenciales. Muy a menudo sucede que algún cliente potencial llame y solicite un servicio en particular que su empresa atiende y, a veces, esto se olvida. Con CRM nunca olvidará a sus clientes potenciales y podrá gestionarlos a todos en un solo lugar.

## Crear Un Nuevo Cliente Potencial

- Navegue a la vista de clientes potenciales desde el menú principal en el lado izquierdo y haga clic en el botón nuevo cliente potencial.

- **Si marca el cliente potencial como público, será visible para todos los miembros del personal** ; de lo contrario, será visible para el miembro del personal asignado y los administradores.

- Los clientes potenciales pueden crear todos los miembros del personal, los miembros del personal que no son administradores **podrán ver los clientes potenciales** en los siguientes casos:

    - Si el cliente potencial se crea a partir del propio miembro del personal.
    - Si el plomo está marcado es público.
    - Si el cliente potencial no se crea a partir de un miembro del personal, pero este miembro del personal está asignado a este cliente potencial.

![imagen clientePotencial](img/img_1.png)

## Agreagar fuentes

Siempre necesitará una forma de saber de dónde obtuvo el cliente pootencial.

- Vaya a **Configuración -> Clientes potenciales -> Fuentes** y agregue una nueva fuente según sus necesidades.

- Para configurar la fuente de clientes potenciales predeterminada, vaya a **Configuración-> Configuración-> Clientes** potenciales y elija Fuente predeterminada.

Puede asignar una fuente por cliente potencial y sabrá de dónde vienen sus clientes potenciales y de dónde tiene la mayoría de los clientes potenciales convertidos en clientes al generar el informe.
La fuente de cliente potencial predeterminada se utiliza para completar automáticamente el campo de origen al crear un nuevo cliente potencial , cada vez que agregue un nuevo cliente potencial, esta fuente se seleccionará automáticamente.

## Estados De Clientes Potenciales

Puede personalizar fácilmente los estados de los clientes potenciales.

- Para agregar un nuevo estado de cliente potencial, deberá ir a **Configuración -> Clientes potenciales -> Estados** y hacer clic en Nuevo estado de cliente potencial .

- Para configurar el estado predeterminado del cliente potencial, vaya a **Configuración-> Configuración-> Clientes** potenciales y elija Estado predeterminado.

El estado de cliente potencial predeterminado se utiliza para completar automáticamente el campo de estado al crear un nuevo cliente potencial , cada vez que agregue un nuevo cliente potencial, este estado se seleccionará automáticamente.
Hay un estado de cliente potencial predeterminado que se puede eliminar con la instalación.

El estado de cliente potencial se utiliza principalmente para informes, para reconocer que un cliente potencial específico se convierte en cliente.

**NOTA** : Después de convertir el cliente potencial en cliente, este cliente potencial se moverá automáticamente al estado Cliente, no cambie el nombre de este estado a otro nombre como NUEVO para evitar confusiones.

## Usando visualizar como Kan Ban

Hay 2 opciones que enumeran los prospectos kan ban y la tabla. Las opciones de kan ban es simple, puede arrastrar y soltar clientes potenciales en un estado específico con un solo clic.

- Para ajustar el límite de clientes potenciales de kan ban por estado, puede ir a Configuración-> Configuración-> Clientes potenciales-> Límite de filas de clientes potenciales kan ban por estado .

- Para ajustar la clasificación predeterminada de kan ban cuando accede a esta vista, puede ir a  Configuración-> Configuración-> Clientes potenciales-> Clientes potenciales predeterminados ordenar kan ban .

## Asigne El Cliente Potencial A Un Miembro Del Personal

Asigne El Cliente Potencial A Un Miembro Del Personal.
 1027 visualizaciones  7 de febrero de 2016 15 de octubre de 2016 mstojanov  5

Abra un cliente potencial y simplemente seleccione el miembro del personal al que desea asignar el cliente potencial.

Después de enviar, el miembro del personal recibirá una notificación de que ha asignado este cliente potencial.

**Todos pueden gestionar clientes potenciales. Pero solo los administradores pueden ver todos los clientes potenciales. Otros miembros del personal que no son administradores pueden ver los clientes potenciales que se le asignaron o si este miembro del personal creó este cliente potencial sin importar a quién se le haya asignado.**

## Registro De Actividad De Clientes Potenciales

Para rastrear la actividad del cliente potencial, abra un modal de cliente potencial y haga clic en la pestaña Registro de actividad.

Todas las acciones importantes se guardarán como registro de actividad para este cliente potencial.

**¿Cuándo se registrará la actividad de un cliente potencial específico?**

- Después de que se crea el plomo.

- Al hacer una nota y hacer clic en contactado hoy, se registrará una actividad en la que se comunicó con el cliente

- potencial en la fecha específica elegida del calendario.

- Adjunto agregado. 

- Adjunto eliminado. 

- Cuando se asigna el plomo a un miembro del personal.

- Cuando el estado del cliente potencial se cambia de kanban con arrastrar y soltar o manualmente desde el modal.

- Cliente potencial marcado / no marcado como perdido

- Cliente potencial marcado / no marcado como basura

![imagen clientePotencial](img/img_2.png)

## Convertir Cliente Potencial En Cliente

1. Haga clic en el cliente potencial y espere que se abra el modal; en el botón superior, haga clic en Convertir en cliente.
2. Se mostrará el modo de conversión a cliente y el sistema intentará completar automáticamente algunos campos como nombre, apellido ...
3. Si los campos personalizados encontrados para el cliente potencial también se mostrarán en el modo de conversión a cliente, tendrá la capacidad de combinar los campos personalizados en el perfil del cliente como campos personalizados o campos de base de datos.

**NOTA** : Si se fusiona como un campo personalizado y este campo no existe, se creará automáticamente.

![imagen clientePotencial](img/img_3.png)

## Generar Informe De Clientes Potenciales

Para ver su informe de clientes potenciales, vaya al menú Informes del menú lateral y haga clic en Clientes potenciales .

Puede ver los siguientes informes:

- Semanal
- Fuentes
- Mensual
- Personal
 

**NOTA** : Si desea que los informes sean precisos, debe asignar el cliente potencial al estado predeterminado del sistema Cliente si tiene conversión.

