# Proyecto

Gestione y facture proyectos con la potente función de gestión de proyectos.
Realice un seguimiento del tiempo dedicado a las tareas y facture a sus clientes. Capacidad para asignar tareas a varios miembros del personal y realizar un seguimiento del temporizador por personal asignado.

Agregue seguidores de tareas incluso si el personal no es miembro del proyecto. El miembro del personal podrá realizar un seguimiento del progreso de la tarea sin acceder al proyecto.

## Vincular Manualmente La Factura Al Proyecto

Vincular la factura al proyecto manualmente sin ir primero al área del proyecto y hacer clic en el botón Proyecto de factura.

- Vaya a Menú -> **Ventas -> Facturas** y haga clic en Crear  nueva factura
- Seleccionar cliente.
- Se mostrará un menú desplegable con proyectos debajo del campo de selección de clientes (si no hay proyectos asociados con el cliente seleccionado, no se mostrará el menú desplegable con proyectos)
- Seleccione el proyecto
- Configure la factura según sus necesidades
- Guardar la factura
- Navegue hasta el área del proyecto y haga clic en la pestaña Facturas , podrá ver la factura recién creada asociada con su proyecto seleccionado.

![imagen proyectos](img/img_1.png)

## Tiempos

Los tiempo del proyecto son cronómetros de inicio para tareas relacionadas con un proyecto específico.

Para ver todas las hojas de tiempo del proyecto mientras ve el proyecto, haga clic en la pestaña de hojas de tiempo .

![imagen proyectos](img/img_2.png)

Puede ver y ordenar todos los temporizadores que están en progreso o detenidos, todas las hojas de tiempo están vinculadas al miembro del personal y la tarea.

**Agregar nueva hoja de tiempo manualmente**

Mientras esté en la pestaña tiempo, siga los pasos:

Haga clic en el botón registro de horas

![imagen proyectos](img/img_3.png)

- Seleccione la hora de inicio y la hora de finalización en el calendario.
- Seleccione la tarea a la que se vinculará esta hoja de tiempo.
- Seleccionar miembro del personal
    - De forma predeterminada, el usuario que inició sesión se seleccionará automáticamente
    - Si el usuario no tiene permiso para los proyectos, EDITAR o CREAR, solo el usuario que inició sesión estará disponible para seleccionarlo como personal de la hoja de tiempo en el menú desplegable

![imagen proyectos](img/img_4.png)

## Creando Nuevo Proyecto

Para crear un nuevo proyecto, vaya a Proyectos en el menú lateral y haga clic en **Nuevo proyecto** en el botón superior izquierdo.

- Nombre del proyecto: el nombre del proyecto: visible para el cliente
- Seleccionar cliente
- Tipo de facturación: hay 3 tipos de facturación para los proyectos. (seleccionado automáticamente del tipo de facturación más utilizado)  NOTA: El tipo de facturación no se puede cambiar si se encuentran tareas facturadas para el proyecto.
    - Costo fijo
    - Horas del proyecto: basado en el total de horas facturables del proyecto
    - Horas de tareas: según el total de horas facturables de las tareas: el total se calcula por tarea Tarifa por hora ( al crear una tarea, debe establecer la tarifa por hora para estas tareas si es facturable )
- Seleccionar miembros del proyecto: solo los miembros del proyecto y el personal que tenga permiso para administrar proyectos podrán acceder al proyecto.
- Fecha de inicio
- Fecha final
- Descripción del Proyecto

**Configuración del proyecto**

Cada proyecto puede tener su propia configuración en función del cliente.

- Permitir que el cliente vea las tareas
- Permitir que el cliente comente las tareas del proyecto: no se aplica si la opción Permitir que el cliente vea las tareas no está marcada .
- Permitir que el cliente vea los comentarios de las tareas: no se aplica si Permitir que el cliente vea las tareas está desmarcado .
- Permitir que el cliente vea los archivos adjuntos de la tarea: no se aplica si Permitir que el cliente vea las tareas está desmarcado .
- Permitir que el cliente vea los elementos de la lista de verificación de tareas: no se aplica si Permitir que el cliente vea las tareas está desmarcado .
- Permitir que el cliente cargue archivos adjuntos en las tareas: no se aplica si Permitir que el cliente vea las tareas está desmarcado .
- Permitir al cliente ver el tiempo total registrado de la tarea
- Permitir que el cliente vea la descripción general de las finanzas   (la descripción general de las finanzas para las tareas no se muestra si el tipo de facturación del proyecto es Costo fijo)
- Permitir que el cliente cargue archivos
- Permitir al cliente abrir discusiones
- Permitir al cliente ver los hitos
- Permitir que el cliente vea Gantt
- Permitir que el cliente vea las hojas de horas
- Permitir que el cliente vea el registro de actividad
- Permitir que el cliente vea a los miembros del equipo

![imagen proyectos](img/img_5.png)

**Notas**:

- Al crear un nuevo proyecto inicialmente, la configuración predeterminada del proyecto se tomará del último proyecto . Puede ajustar si se requieren cambios.

- La moneda del proyecto será su moneda base o la moneda del cliente . Si el cliente ha configurado una moneda diferente a la moneda base, se utilizará la moneda del perfil del cliente.
