# CRM-DOCUMENTATION

# Intalacion de la Documentación

Esta documentación esta hecha con MKDOCS

# Intalando Python
Para intalar MKDOCS es necesario Python 3.8.2 o superior


Arch Linux: 

- sudo pacman -S python


Ubuntu: 

- sudo apt install python3


Windows:

Descarga el intalador de Python desde la Pagina Oficial

Recuerde agregar Python a sus variables de entorno




# Instalando PIP
Para instalar MKDOCS se requiere instalar PIP  20.0.2 o superior
Una vez intalado Python solo necesitas descargar el archivo
get-pip.py y ejecutar el siguiente comando:
python /directorio/del/archivo/get-pip.py

Nota:

En Windows se deben utilizar barras invertidad (\) para las rutas, y el comando se ejecuta como adminitrador
En Ubuntu ejecutar el python3 en vez de python



Intalando MKDOCS

Una vez instalado Python y PIP se puede instalar MKDOCS con el siguiente comando:
- pip install mkdocs

Corriendo la Documentación

Para correr la docuementación es necesario ejecutar los siguientes comandos:

- cd /directorio/raiz/del/proyecto/documentación
- mkdocs serve #Para ejecutar un servidor con la documentación
- mkdocs build #Para contruir los archivos html
